<?php
require_once './pdo.php';

if (empty($_GET['table']) || empty($_GET['field'])) {
    if (empty($_POST['field']) || empty($_POST['newfield']) || empty($_POST['fieldtype'])) {
        header("HTTP/1.0 404 Not Found");
        exit;
    }
}

try {
    $pdo = new PDO($pdo_string, $pdo_user, $pdo_pwd);
} catch (PDOException $e) {
    print "Error!: " . $e->getMessage() . "<br/>";
    die();
}
?>
<html>
    <head>
        <title>Micro MySQL Adminer</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    </head>
    <body>
    <div class="container">
        <h1>Редактирование поля</h1>
<?php
if (empty($_POST['submit'])) {
    $sql = 'SHOW COLUMNS FROM `' . $_GET['table'] . '` WHERE field = :fieldname;';
    $args = [':fieldname' => $_GET['field']];
    try {
        $sth = $pdo->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        if ($sth->execute($args)) {
            $field = $sth->fetch();
?>
            <form action="editfield.php" method="post">
            <input type="hidden" name="table" value="<?= $_GET['table'] ?>">
            <input type="hidden" name="field" value="<?= $_GET['field'] ?>">
            <div class="form-group">
                <label for="newfield">Имя поля:</label>
                <input type="text" name="newfield" value="<?= $_GET['field'] ?>">
            </div>
            <div class="form-group">
                <label for="newfield">Тип поля:</label>
                <input type="text" name="fieldtype" value="<?= $field['Type'] ?>">
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" name="submit" value="Сохранить">
                <a href="showtable.php?table=<?= $_GET['table'] ?>" class="btn btn-default">Вернуться в таблице</a>
            </div>
            </form>
<?php
        } else {
            echo '<div class="alert alert-danger">Что-то пошло не так... :(</div>';
            echo '<a href="showtable.php?table=' . $_GET['table'] . '" class="btn btn-primary">Вернуться в таблице</a>';
        }
    } catch (PDOException $e) {
        print "Error!: " . $e->getMessage() . "<br/>";
        die();
    }
} else {
    if (empty($_POST['field']) && empty($_POST['newfield']) && empty($_POST['fieldtype'])) {
        echo '<div class="alert alert-danger">Не корректный запрос</div>';
    } else {
        $sql = 'ALTER TABLE `' . $_POST['table'] . '`  CHANGE `' . $_POST['field'] . '` `' . $_POST['newfield'] . '` ' . $_POST['fieldtype'] . ';';
        $sth = $pdo->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        if ($sth->execute()) {
            echo '<div class="alert alert-success">Поле `' . $_POST['field'] . '` отредактировано успешно</div>';
        } else {
            echo '<div class="alert alert-danger">Поле `' . $_POST['field'] . '` не удалось отредактировать</div>';
        }
    }
    echo '<a href="showtable.php?table=' . $_POST['table'] . '" class="btn btn-primary">Вернуться в таблице</a>';
}
?>
    </div>
    </body>
</html>
