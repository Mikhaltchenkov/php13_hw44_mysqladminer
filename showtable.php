<?php
require_once './pdo.php';

if (empty($_GET['table'])) {
    header("HTTP/1.0 404 Not Found");
    exit;
}

try {
    $pdo = new PDO($pdo_string, $pdo_user, $pdo_pwd);
} catch (PDOException $e) {
    print "Error!: " . $e->getMessage() . "<br/>";
    die();
}
?>
<html>
<head>
    <title>Micro MySQL Adminer</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <?php
$sql = 'SHOW COLUMNS FROM ' . $_GET['table'];
try {
    $sth = $pdo->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    $sth->execute();
    echo '<h1>Таблица ' . $_GET['table'].'</h1>';
    ?>
<table class="table table-hover">
    <thead>
    <tr>
        <th>
            Field
        </th>
        <th>
            Type
        </th>
        <th>
            Null
        </th>
        <th>
            Key
        </th>
        <th>
            Default
        </th>
        <th>
            Extra
        </th>
        <th>
            Операции
        </th>
    </tr>
    </thead>
    <tbody>
<?php
    foreach ($sth->fetchAll() as $column) {
        echo '<tr><td>'.$column['Field'] . '</td><td>' . $column['Type'] . '</td>'.
            '<td>' . $column['Null'] . '</td><td>' . $column['Key'] . '</td>'.
            '<td>' . $column['Default'] . '</td><td>' . $column['Extra'] . '</td>'.
            '<td><a href="editfield.php?table=' . $_GET['table'] . '&field=' . $column['Field'] . '" class="btn btn-info">Редакировать</a> '.
            '<a href="removefield.php?table=' . $_GET['table'] . '&field=' . $column['Field'] . '" class="btn btn-danger">Удалить</a></td></tr>';
    }
    ?>
    </tbody>
</table>
    <?php
} catch (PDOException $e) {
    print "Error!: " . $e->getMessage() . "<br/>";
    die();
}
?>
    <a class="btn btn-primary" href="index.php">Вернуться к списку</a>
</div>
</body>
</html>
