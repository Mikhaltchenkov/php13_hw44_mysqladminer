<html>
<head>
    <title>Micro MySQL Adminer</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
<div class="container">
<h1>Список таблиц</h1>
    <ol>
<?php
require_once './pdo.php';

try {
    $pdo = new PDO($pdo_string, $pdo_user, $pdo_pwd);
} catch (PDOException $e) {
    print "Error!: " . $e->getMessage() . "<br/>";
    die();
}

$sql = 'SHOW TABLES;';
try {
    $sth = $pdo->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    $sth->execute();
    foreach ($sth->fetchAll() as $table) {
        echo '<li><a href="showtable.php?table=' . $table[0] . '">'. $table[0] . '</a></li>';
    }
} catch (PDOException $e) {
    print "Error!: " . $e->getMessage() . "<br/>";
    die();
}
?>
    </ol>
    <a href="createtable.php" class="btn btn-default">Создать таблицу 'test'</a>
</div>
</body>
</html>
