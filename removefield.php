<?php
require_once './pdo.php';

if (empty($_GET['table']) || empty($_GET['field'])) {
    header("HTTP/1.0 404 Not Found");
    exit;
}

try {
    $pdo = new PDO($pdo_string, $pdo_user, $pdo_pwd);
} catch (PDOException $e) {
    print "Error!: " . $e->getMessage() . "<br/>";
    die();
}
?>
<html>
    <head>
        <title>Micro MySQL Adminer</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    </head>
    <body>
    <div class="container">
        <h1>Удаление поля</h1>
<?php

$sql = 'ALTER TABLE `' . $_GET['table'] .'`  DROP COLUMN `' . $_GET['field'] . '`;';
try {
    $sth = $pdo->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    if ($sth->execute()) {
        echo '<div class="alert alert-success">Поле `' . $_GET['field'] . '` удалено успешно</div>';
    } else {
        echo '<div class="alert alert-danger">Поле `' . $_GET['field'] . '` не удалось удалить</div>';
    }
    echo '<a href="showtable.php?table=' . $_GET['table'] . '" class="btn btn-primary">Вернуться в таблице</a>';
} catch (PDOException $e) {
    print "Error!: " . $e->getMessage() . "<br/>";
    die();
}

?>
    </div>
    </body>
</html>
